import os
import logging
import asyncio
from aiogram import Bot, Dispatcher, executor, types
from async_get_data import create_data_tasks


logging.basicConfig(level=logging.INFO)


API_TOKEN: str = os.environ.get("TOKEN")
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=["start"])
async def process_start_command(message: types.Message):
    await message.reply(
        "Привет!\nЯ могу посчитать среднюю зарплату по запросу "
        "на основе данных с hh.ru.\nНапишите мне /salary для старта!"
    )


@dp.message_handler(commands=["help"])
async def process_start_command(message: types.Message):
    await message.reply(
        "Введите /start, чтобы начать поиск."
    )


@dp.message_handler(commands=["salary"])
async def handle_text(message):
    await bot.send_message(message.chat.id, "Введите название вакансии для поиска.")

    @dp.message_handler(content_types=["text"])
    async def handle_text(message):
        user_input = message.text
        await bot.send_message(message.chat.id, "Ожидайте, ищу информацию...")
        try:
            await message.reply(await create_data_tasks(user_input))
        except Exception as ex:
            logging.error(
                f"Что-то пошло не так. Значение 'user input': {user_input}. Ошибка: {ex}"
            )
            await message.reply(
                "Ничего не удалось найти. Попробуйте сфомрулировать запрос иначе.\
                 \nНапример: junior python developer"
            )


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)
