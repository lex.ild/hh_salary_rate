import json
import logging
import os
from datetime import datetime

import aiohttp
import asyncio
import pandas as pd

from sqlalchemy import Column, Date, String, text
from sqlalchemy.orm import declarative_base
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker


# API HH отдаёт максимум 2к вакансий в бесплатной версии, в данной конфигурации 20 страниц
HH_PAGE_LIMIT: int = 20

SALARY_LIST: list = []

logger = logging.getLogger(__name__)

c_handler = logging.StreamHandler()
f_handler = logging.FileHandler("file.log")
c_handler.setLevel(logging.INFO)
f_handler.setLevel(logging.ERROR)

c_format = logging.Formatter("%(name)s - %(levelname)s - %(message)s")
f_format = logging.Formatter(
    "%(asctime)s - %(name)s -\
         %(levelname)s - %(message)s"
)
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)

logger.addHandler(c_handler)
logger.addHandler(f_handler)


Base = declarative_base()


# описываем таблицу в БД
class UsdRate(Base):
    __tablename__ = "usd_rate"
    date = Column(Date, primary_key=True)
    usd = Column(String, primary_key=True)


async def get_cb_usd() -> float:
    """Получаем курс доллара по API ЦБ РФ"""
    url = 'https://www.cbr-xml-daily.ru/daily_json.js'
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            response_json = await response.read()
            data = json.loads(response_json)
            return data['Valute']['USD']['Value']


async def get_usd_rate() -> float:
    """В зависимости от текущей даты берём курс из БД, или запрашиваем актуальный по API"""
    db_uri = os.environ.get('POSTGRES_URI')
    engine = create_async_engine(db_uri, echo=True)

    async_session = async_sessionmaker(engine, expire_on_commit=False)
    async with async_session() as session:
        async with session.begin():
            # берём последнюю дату и значение usd
            query = 'SELECT usd FROM usd_rate ORDER BY date DESC LIMIT 1;'
            last_rate = await session.execute(text(query))

            max_date_query = 'SELECT MAX(date) FROM usd_rate;'
            max_date = await session.execute(text(max_date_query))
            current_date = datetime.now().date()

            # если дата последней записи из БД неактуальна, то запрашиваем в ЦБ новый курс и сохраняем
            if current_date > max_date.first()[0]:
                current_rate = await get_cb_usd()

                new_rate = await session.execute(
                    text(f"INSERT INTO usd_rate (date, usd) VALUES ('{current_date}', '{current_rate}');"))
                await engine.dispose()
                return current_rate
            # если дата актуальна, то возвращаем последнее значение
            else:
                await engine.dispose()
                return float(last_rate.first()[0])


async def get_data(session: aiohttp.ClientSession, page: int, vacancy_text: str, current_usd_rate: float):
    """Делаем запрос к API hh.ru, получаем список вакансий с зарплатами, считаем среднее по вилке"""
    headers: dict = {
        "User-Agent": "HH_salary-rate",
        "Content-Type": "grant_type=client_credentials;",
    }

    url = (
        f"https://api.hh.ru/vacancies?text={vacancy_text}&page={page}"
        f"&per_page=100&only_with_salary=true&search_field=name"
    )
    async with session.get(url) as r:

        if r.status != 200:
            logger.error(
                f"Ошибка получения данных от API': {vacancy_text} Статус-код: {r.status}"
            )
            return "Ошибка получения данных. Попробуйте сделать запрос позже."

        data = await r.json()

        items = data["items"]
        try:
            for item in items:
                salary = item["salary"]
                min_salary = salary["from"]
                max_salary = salary["to"]
                if min_salary is None:
                    if salary["currency"] == "USD":
                        mid = round(max_salary * current_usd_rate)
                    else:
                        mid = salary["to"]
                elif max_salary is None:
                    if salary["currency"] == "USD":
                        mid = round(min_salary * current_usd_rate)
                    else:
                        mid = min_salary
                else:
                    if salary["currency"] == "USD":
                        mid = int((min_salary + max_salary) / 2 * current_usd_rate)
                    else:
                        mid = int((min_salary + max_salary) / 2)

                SALARY_LIST.append(mid)

        except KeyError as keyex:
            logger.error(
                f"Что-то пошло не так. Значение 'user input': {vacancy_text}. Ошибка: {keyex}"
            )


async def create_data_tasks(vacancy_text="") -> str:
    """Создаём таски для каждой страницы в рамках лимита и запускаем, в итоге получаем summary по вакансиям"""
    async with aiohttp.ClientSession() as session:
        tasks = []
        current_usd_rate = await get_usd_rate()

        for page in range(HH_PAGE_LIMIT):
            task = asyncio.create_task(get_data(session, page, vacancy_text, current_usd_rate))
            tasks.append(task)
        await asyncio.gather(*tasks)

        pd_series = pd.Series(SALARY_LIST)

        summary = (
            f"Проверено {len(SALARY_LIST)} вакансий\n"
            f"Средняя зарплата: {int(pd_series.mean())} рублей\n"
            f"Медианная: {int(pd_series.median())} рублей"
        )
        SALARY_LIST.clear()
        return summary

