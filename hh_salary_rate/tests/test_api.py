import json
import os.path
import unittest
from unittest.mock import patch

from hh_salary_rate.async_get_data import get_cb_usd, get_data, create_data_tasks

DATA_DIR = os.path.join(os.path.dirname(__file__), 'test_data')


class ApiTestCase(unittest.TestCase):
    @patch('hh_salary_rate.get_data_hh.requests.get')
    def test_get_cb_usd(self, mock_get):
        with open(os.path.join(DATA_DIR, 'expected_usd_response.json')) as file:
            data = json.load(file)

        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = data

        response = get_cb_usd()

        self.assertEqual(response, 68.676)

    @patch('hh_salary_rate.get_data_hh.requests.get')
    def test_bad_get_cb_usd(self, mock_get):
        mock_get.return_value.status_code = 403

        with self.assertRaises(ConnectionError):
            response = get_cb_usd()

    @patch('hh_salary_rate.get_data_hh.get_usd_rate')
    @patch('hh_salary_rate.get_data_hh.requests.get')
    def test_api_hh(self, mock_get, mock_get_usd):
        with open(os.path.join(DATA_DIR, 'expected_hh_response.json')) as file:
            data = json.load(file)

        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = data
        mock_get_usd.return_value = 68.676

        resp = get_data('junior python')

        self.assertEqual(resp, 'Проверено 200 вакансий\nСредняя зарплата: 86867 рублей\nМедианная: 80000 рублей')

    @patch('hh_salary_rate.get_data_hh.get_usd_rate')
    @patch('hh_salary_rate.get_data_hh.requests.get')
    def test_bad_api_hh(self, mock_get, mock_get_usd):
        mock_get.return_value.status_code = 403
        mock_get_usd.return_value = 68.676

        resp = get_data('junior python')

        self.assertEqual(resp, 'Ошибка получения данных. Попробуйте сделать запрос позже.')

