Проект сделан в учебных целях.

Пользователь в чат-боте telegram может ввести ключевые слова и получить по выборке вакансий среднюю и медианную зарплату.
Происходит обращение к API hh.ru по ключевым словам. Вакансии с зарплатой в долларах пересчитываются по актуальному курсу, данные запрашиваются по API в ЦБ. Чтобы постоянно не запрашивать курс по API, данные на текущий день сохраняются в БД и берутся оттуда.

В приложении используются: aiogram, SQLAlchemy, asyncio, aiohttp, postgres, pandas.

Посмотреть в работе можно здесь: https://t.me/SalaryRate_bot